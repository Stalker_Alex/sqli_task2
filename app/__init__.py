from flask import Flask
from config import Config
import sqlite3
from flask_bootstrap import Bootstrap
import mysql.connector

app = Flask(__name__)
app.config.from_object(Config)
bootstrap = Bootstrap(app)




def connection():
    config = {
        'user': 'root',
        'password': 'root',
        'host': 'db',
        'port': '3306',
        'database': 'knights'
    }
    conn = mysql.connector.connect(**config)

    return conn


from app import routes
