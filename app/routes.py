from app import app
from flask import render_template, redirect, url_for, request, flash, session, escape
from app.forms import LoginForm, RegisterForm, NoteForm
from app import connect
from random import random
import hashlib


@app.route('/')
@app.route('/index')
def index():
    c = connect().cursor()
    c.execute("SELECT title, text FROM notes WHERE prv = 0")
    posts = c.fetchall()
    return render_template('index.html', posts=posts)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if request.method == "POST" and form.validate():
        username = form.username.data
        password = form.password.data
        find_user = "SELECT * FROM users WHERE username = ? and PASSWORD = ?"
        cursor = connect().cursor()
        cursor.execute(find_user, [username, password])
        results = cursor.fetchall()
        if results:
            session['username'] = username
            return redirect(url_for('index'))
        else:
            print("incorrect login or password")
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    session.pop('username', None)
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    try:
        form = RegisterForm()

        if request.method == "POST" and form.validate():
            username = form.username.data
            password = form.password.data
            c = connect()

            x = c.execute("SELECT * FROM users WHERE username = ?", (username,))

            if x == username:
                flash("That username is already taken, please choose another")
                return render_template('register.html', form=form)

            else:
                user_id = random()
                session['username'] = username
                user_session = session['username']
                c.execute('''INSERT INTO users(username, password, user_id, user_session) VALUES (?,?,?,?)''',
                          (username, password, user_id, user_session))
                c.commit()
                c.close()
                flash("Thanks for registering!")

                return redirect(url_for('index'))

        return render_template("register.html", form=form)

    except Exception as e:
        return (str(e))


@app.route('/create', methods=['GET', 'POST'])
def create():
    try:
        form = NoteForm()
        if request.method == "POST" and form.validate():
            title = form.title.data
            body = form.body.data
            prv = form.private.data
            c = connect()
            note_id = random()
            user_id = session['username']
            c.execute('''INSERT INTO notes(text, title, prv, note_id, user_id) VALUES (?,?,?,?,?)''',
                      (str(body), str(title), bool(prv), str(note_id), str(user_id),))
            c.commit()
            c.close()
        return render_template("create.html", form=form)
    except Exception as e:
        return (str(e))


@app.route('/My_notes')
def my_notes():
    c = connect().cursor()
    c.execute("SELECT title, text FROM notes WHERE user_id = ?", (session['username'],))
    posts = c.fetchall()
    return render_template("My_notes.html", posts=posts)


@app.route('/Find_notes', methods=['GET', 'POST'])
def find_notes():

    c = connect().cursor()
    q = request.args.get('q')
    if q:
        c.execute("SELECT title, text FROM notes WHERE title LIKE '{0}' AND prv = 0;".format(q))
        posts = c.fetchall()
    else:
        c.execute("SELECT title, text FROM notes WHERE prv = 0")
        posts = c.fetchall()
    return render_template('Find_notes.html', posts=posts)
