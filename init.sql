CREATE DATABASE Secnote;
use Secnotes;

CREATE TABLE IF NOT EXISTS users(
    user_id VARCHAR(50) NOT NULL,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    user_session VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS notes(
    user_id VARCHAR(50) NOT NULL,
    note_id VARCHAR(50) NOT NULL,
    text VARCHAR(50) NOT NULL,
    title VARCHAR(50) NOT NULL,
    prv BIT
);

insert into users
    (user_id, username, password, user_session)
VALUES
    ('admin', 'admin', 'admin', 'admin');
insert into notes
VALUES
    ('admin', 'admin', 'M*CTF{Th4t_w4s_an_3asy_0n3}', 'FLAG');